import React from "react";
import './Login.scss';
import { Link } from 'react-router-dom';


export class Login extends React.Component {
    render() {
        return (
            <div className="login-container">
                <h2>USER AREA</h2>
                <form>
                    <div className="form-field">
                        <label >
                            USERNAME:
                        <input style={{ display: 'block' }} type="text" name="username" />
                        </label>
                    </div>
                    <div className="form-field">
                        <label >
                            PASSWORD:
                        <input style={{ display: 'block' }} type="password" name="password" />
                        </label>
                    </div>
                    <div>
                        <button className="login-button">
                            <Link to="/userPreferences">
                                LOGIN
                            </Link>
                        </button>

                    </div>
                </form>
            </div>
        );
    }
}

export default Login;