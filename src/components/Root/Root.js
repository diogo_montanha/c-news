import React from "react";
import { Row, Col } from 'reactstrap';
import './Root.scss';
import logo from '../../assets/logo.png';
import burgerMenu from '../../assets/menu.png';
import MenuOptions from "../MenuOptions/MenuOptions";
import Main from "../Main/Main";

class Root extends React.Component {

    constructor(props) {
        super(props);
        this.state = { windowWidth: window.innerWidth };
    }

    updateDimensions = () => {
        this.setState({ windowWidth: window.innerWidth })
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    showMenuBar = () => {
        this.refs.burgerMenuOptions.show();
    }

    render() {
        this.showBurgerMenu = this.state.windowWidth <= 750;

        this.getBurgerMenu = () => {
            return (
                <Col xs="3" id="burger-menu">
                    <img alt="Menu Bar" onClick={this.showMenuBar} src={burgerMenu} />
                </Col>
            )
        }

        this.getBurgerMenuOptions = () => {
            return (
                <MenuOptions ref="burgerMenuOptions" className="burger-menu-options"></MenuOptions>
            )
        }

        this.getMainComponent = () => {
            return (
                <Row >
                    {this.showBurgerMenu ? this.getBurgerMenuOptions() : null}
                    <Main></Main>
                </Row>
            )
        }

        this.getDefaultMenu = () => {
            return (
                <Col xs="9" id="col-menu">
                    <MenuOptions className="nav-list pull-right"></MenuOptions>
                </Col>
            )
        }

        return (
            <div>
                <Row className="nav-menu">
                    {this.showBurgerMenu ? this.getBurgerMenu() : null}
                    <Col xs="3" id="col-logo">
                        <img alt="Logo" className="logo" src={logo} />
                    </Col>
                    {this.showBurgerMenu ? null : this.getDefaultMenu()}
                </Row>
                <div>
                    {this.getMainComponent()}
                </div>
            </div>
        )

    };
}

export default Root;