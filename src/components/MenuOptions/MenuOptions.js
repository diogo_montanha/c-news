import React from "react";
import './MenuOptions.scss';
import { Link } from 'react-router-dom';

class MenuOptions extends React.Component {

    componentDidMount() {
        document.querySelector("#menu-list").className += this.props.className;
        document.querySelector("#menu-container").className = this.props.className === 'nav-list pull-right' ? 'visible' :'invisible';
    }

    constructor(props) {
        super(props)
        this.state = { visible:  this.props.className === 'nav-list pull-right'}
    }

    show() {
        this.setState({ visible: this.props.className === 'nav-list pull-right' ? true : !this.state.visible });
        document.querySelector("#menu-container").className = this.state.visible ? 'visible' : 'invisible';
    }

    hide() {
        this.setState({ visible: false });
        document.querySelector("#menu-container").className = this.state.visible ? 'visible' : 'invisible';
    }

    render() {
        return (
            <div id="menu-container" >
                <ul id="menu-list">
                    <li>
                        <Link className="nav-item" to="/">
                            POLITICS
                        </Link>
                    </li>
                    <li>
                        <Link className="nav-item" to="/">
                            BUSINESS
                    </Link>
                    </li>
                    <li>
                        <Link className="nav-item" to="/">
                            TECH
                    </Link>
                    </li>
                    <li >
                        <Link className="nav-item" to="/">
                            SCIENCE
                    </Link>
                    </li>
                    <li>
                        <Link className="nav-item" to="/">
                            SPORTS
                    </Link>
                    </li>
                    <li id="login-link">
                        <Link className="nav-item" to="/login">
                            LOGIN
                    </Link>
                    </li>
                </ul>

            </div>
        )
    }
}

export default MenuOptions;