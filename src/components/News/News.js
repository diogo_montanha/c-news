import React from "react";
import './News.scss';
import { Row, Col } from 'reactstrap';
import author_image from '../../assets/p-the-office-creed-bratton.jpg';



class News extends React.Component {

    render() {
        return (
            <div>
                <Row style={{ marginBottom: 8 + 'px' }}>
                    <Col xs="12">
                        <span style={{color: this.props.newsContent.color}} className="news-type">
                            {this.props.newsContent.type}
                        </span>
                    </Col>
                </Row>
                {
                    this.props.newsContent.image ?
                        <Row style={{ marginBottom: 15 + 'px' }}>
                            <Col >
                                <img alt="News" style={{
                                    maxWidth: 100 + '%',
                                    maxHeight: 100 + '%'
                                }}
                                    src={this.props.newsContent.image} />
                            </Col>
                        </Row> :
                        null
                }
                <Row style={{ marginBottom: 10 + 'px' }}>
                    <Col >
                        <span style={{ fontSize: this.props.newsContent.titleSize }} >
                            {this.props.newsContent.title}
                        </span>
                    </Col>
                </Row>
                <Row style={{ marginBottom: 20 + 'px' }}>
                    <Col >
                        <img alt="Author" className="author-image" src={author_image} />
                        <span className="author-name">
                            by Creed Bratton
                        </span>
                    </Col>
                </Row>
                <Row>
                    <Col >
                        <p className="news-text">
                            {this.props.newsContent.text}
                        </p>
                    </Col>
                </Row>


            </div>
        )
    }
}

export default News;