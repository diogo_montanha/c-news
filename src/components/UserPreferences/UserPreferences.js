import React from "react";
import './UserPreferences.scss';
import { Link } from 'react-router-dom';
import { Row } from 'reactstrap';

export class UserPreferences extends React.Component {

    loadUserPreferences() {

    }

    render() {
        return (
            <div className="preferences-container">
                <h2>WELCOME, <a id="username" href-="">USERNAME</a></h2>
                <div className="interest-container">
                    <ul className="interests-list">
                        <li >
                            <h3 className="interests" >MY INTERESTS</h3>

                            <button className="btn-preference btn-politics">
                                POLITICS
                            </button>
                        </li>
                        <li>
                            <button className="btn-preference btn-business">
                                BUSINESS
                        </button>
                        </li>
                        <li>
                            <button className="btn-preference btn-tech">
                                TECH
                        </button>
                        </li>
                        <li>
                            <button className="btn-preference btn-science">
                                SCIENCE
                        </button>
                        </li>
                        <li>
                            <button className="btn-preference btn-sports">
                                SPORTS
                        </button>
                        </li>
                    </ul>
                </div>

                <div >
                    <Row className="button-container">
                        <button className="save-button">
                            SAVE
                    </button>
                    </Row>
                    <Row className="button-container">
                        <Link className="back-button" to="/">
                            BACK TO HOME
                        </Link>
                    </Row>
                </div>



            </div >
        );
    }
}

export default UserPreferences;