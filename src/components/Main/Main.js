import React from "react";
import { Switch, Route } from 'react-router-dom'
import Home from '../Home/Home'
import Login from '../Login/Login'
import UserPreferences from '../UserPreferences/UserPreferences'

export class Main extends React.Component {
    render() {
        return (
            <main style={{width:100 + '%'}}>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/login' component={Login}/>
                    <Route path='/userPreferences' component={UserPreferences}/>
                </Switch>
            </main>
        );
    }
}

export default Main;