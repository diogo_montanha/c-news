import React from "react";
import News from "../News/News";
import { Row, Col } from 'reactstrap';
import './Home.scss';

import news_01 from '../../assets/news_01@2x.png';
import news_02 from '../../assets/news_02@2x.jpg';
import news_03 from '../../assets/news_03@2x.jpg';


export class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = { windowWidth: window.innerWidth };
    }

    updateDimensions = () => {
        this.setState({ windowWidth: window.innerWidth })
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    getSecondaryNews() {
        if (this.state.windowWidth > 1170) {
            return (
                <Row>
                    <Col xs="4">
                        <News newsContent={
                            {
                                type: "SPORTS",
                                title: "For Some Atlanta Hawks, a Revved-Up Game of Uno Is Diversion No. 1",
                                titleSize: 17,
                                color: '#F6A623',
                                text: "The favored in-flight pastime of a group of players including Al Horford, Kent Bazemore and Dennis Schroder is a schoolchildren’s card game with some added twists."
                            }
                        } ></News>
                    </Col>
                    <Col xs="4">
                        <News newsContent={
                            {
                                type: "TECH",
                                title: "Picking a Windows 10 Security Package",
                                titleSize: 17,
                                color: '#4990E2',
                                text: "Oscar the Grouch has a recycling bin and Big Bird has moved to a tree as the children’s classic debuts on HBO, aiming at a generation that doesn’t distinguish between TV and mobile screens."
                            }}></News>
                    </Col>
                    <Col xs="4">
                        <News newsContent={
                            {
                                type: "SCIENCE",
                                title: "As U.S. Modernizes Nuclear Weapons, ‘Smaller’ Leaves Some Uneasy",
                                titleSize: 17,
                                color: '#7CBC37',
                                text: "The Energy Department and the Pentagon have been readying a weapon with a build-it-smaller approach, setting off a philosophical clash in the world of nuclear arms."
                            }}></News>
                    </Col>
                </Row>
            )
        }
        if (this.state.windowWidth > 750) {
            return (
                <Row>
                    <Col xs="6">
                        <News newsContent={
                            {
                                type: "SPORTS",
                                title: "For Some Atlanta Hawks, a Revved-Up Game of Uno Is Diversion No. 1",
                                titleSize: 17,
                                color: '#F6A623',
                                text: "The favored in-flight pastime of a group of players including Al Horford, Kent Bazemore and Dennis Schroder is a schoolchildren’s card game with some added twists."
                            }
                        } ></News>
                    </Col>
                    <Col xs="6">
                        <News newsContent={
                            {
                                type: "TECH",
                                title: "Picking a Windows 10 Security Package",
                                titleSize: 17,
                                color: '#4990E2',
                                text: "Oscar the Grouch has a recycling bin and Big Bird has moved to a tree as the children’s classic debuts on HBO, aiming at a generation that doesn’t distinguish between TV and mobile screens."
                            }}></News>
                    </Col>
                    <Col xs="6">
                        <News newsContent={
                            {
                                type: "SCIENCE",
                                title: "As U.S. Modernizes Nuclear Weapons, ‘Smaller’ Leaves Some Uneasy",
                                titleSize: 17,
                                color: '#7CBC37',
                                text: "The Energy Department and the Pentagon have been readying a weapon with a build-it-smaller approach, setting off a philosophical clash in the world of nuclear arms."
                            }}></News>
                    </Col>
                </Row>
            )
        }
        return (
            <Row>
                <Col xs="12">
                    <News newsContent={
                        {
                            type: "SPORTS",
                            title: "For Some Atlanta Hawks, a Revved-Up Game of Uno Is Diversion No. 1",
                            titleSize: 17,
                            color: '#F6A623',
                            text: "The favored in-flight pastime of a group of players including Al Horford, Kent Bazemore and Dennis Schroder is a schoolchildren’s card game with some added twists."
                        }
                    } ></News>
                </Col>
                <Col xs="12">
                    <News newsContent={
                        {
                            type: "TECH",
                            title: "Picking a Windows 10 Security Package",
                            titleSize: 17,
                            color: '#4990E2',
                            text: "Oscar the Grouch has a recycling bin and Big Bird has moved to a tree as the children’s classic debuts on HBO, aiming at a generation that doesn’t distinguish between TV and mobile screens."
                        }}></News>
                </Col>
                <Col xs="12">
                    <News newsContent={
                        {
                            type: "SCIENCE",
                            title: "As U.S. Modernizes Nuclear Weapons, ‘Smaller’ Leaves Some Uneasy",
                            titleSize: 17,
                            color: '#7CBC37',
                            text: "The Energy Department and the Pentagon have been readying a weapon with a build-it-smaller approach, setting off a philosophical clash in the world of nuclear arms."
                        }}></News>
                </Col>
            </Row>
        )
    }

    getMainNews() {
        if (this.state.windowWidth > 1170) {

            return (
                <Row>
                    <Col xs="6">
                        <News newsContent={
                            {
                                image: news_01,
                                type: "POLITICS",
                                title: "Obama Offers Hopeful Vision While Noting Nation's Fears",
                                titleSize: 29,
                                color: '#FF001F',
                                text: ""
                            }
                        } ></News>
                    </Col>
                    <Col xs="3">
                        <News newsContent={
                            {
                                image: news_02,
                                type: "TECH",
                                title: "Didi Kuaidi, The Company Beating Uber In China, Opens Its API To Third Party Apps",
                                titleSize: 17,
                                color: '#4990E2',
                                text: "One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties."
                            }}></News>
                    </Col>
                    <Col xs="3">
                        <News newsContent={
                            {
                                image: news_03,
                                type: "SCIENCE",
                                title: "NASA Formalizes Efforts To Protect Earth From Asteroids",
                                titleSize: 18,
                                color: '#7CBC37',
                                text: "Last week, NASA announced a new program called the Planetary Defense Coordination Office (PDCO) which will coordinate NASA’s efforts for detecting and tracking near-Earth objects (NEOs). If a large object comes hurtling toward our planet…"
                            }}></News>
                    </Col>
                </Row>
            )
        }
        if (this.state.windowWidth > 750) {
            return (
                <Row>
                    <Col xs="12">
                        <News newsContent={
                            {
                                image: news_01,
                                type: "POLITICS",
                                title: "Obama Offers Hopeful Vision While Noting Nation's Fears",
                                titleSize: 29,
                                color: '#FF001F',
                                text: ""
                            }
                        } ></News>
                    </Col>
                    <Col xs="6">
                        <News newsContent={
                            {
                                image: news_02,
                                type: "TECH",
                                title: "Didi Kuaidi, The Company Beating Uber In China, Opens Its API To Third Party Apps",
                                titleSize: 17,
                                color: '#4990E2',
                                text: "One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties."
                            }}></News>
                    </Col>
                    <Col xs="6">
                        <News newsContent={
                            {
                                image: news_03,
                                type: "SCIENCE",
                                title: "NASA Formalizes Efforts To Protect Earth From Asteroids",
                                titleSize: 18,
                                text: "Last week, NASA announced a new program called the Planetary Defense Coordination Office (PDCO) which will coordinate NASA’s efforts for detecting and tracking near-Earth objects (NEOs). If a large object comes hurtling toward our planet…"
                            }}></News>
                    </Col>
                </Row>
            )
        }
        return (
            <Row>
                <Col xs="12">
                    <News newsContent={
                        {
                            image: news_01,
                            type: "POLITICS",
                            title: "Obama Offers Hopeful Vision While Noting Nation's Fears",
                            titleSize: 17,
                            color: '#FF001F',
                            text: ""
                        }
                    } ></News>
                </Col>
                <Col xs="12">
                    <News newsContent={
                        {
                            image: news_02,
                            type: "TECH",
                            title: "Didi Kuaidi, The Company Beating Uber In China, Opens Its API To Third Party Apps",
                            titleSize: 17,
                            color: '#4990E2',
                            text: "One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties."
                        }}></News>
                </Col>
                <Col xs="12">
                    <News newsContent={
                        {
                            image: news_03,
                            type: "SCIENCE",
                            title: "NASA Formalizes Efforts To Protect Earth From Asteroids",
                            titleSize: 18,
                            color: '#7CBC37',
                            text: "Last week, NASA announced a new program called the Planetary Defense Coordination Office (PDCO) which will coordinate NASA’s efforts for detecting and tracking near-Earth objects (NEOs). If a large object comes hurtling toward our planet…"
                        }}></News>
                </Col>
            </Row>
        )
    }

    render() {
        return (
            <div className="news-container">
                {this.getMainNews()}
                <hr />
                {this.getSecondaryNews()}
            </div>
        );
    }
}

export default Home;